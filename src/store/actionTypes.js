export const LOAD_DISHES_LIST = 'LOAD_DISHES_LIST';
export const REMOVE_DISH_ITEM = 'REMOVE_DISH_ITEM';
export const LOAD_ORDERS_LIST = 'LOAD_ORDERS_LIST';
export const COMPLETE_EXISTING_ORDER = 'COMPLETE_EXISTING_ORDER';