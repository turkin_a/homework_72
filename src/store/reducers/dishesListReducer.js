import * as actionTypes from '../actionTypes';

const initialState = {
  dishList: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_DISHES_LIST:
      return {dishList: action.dishList};
    case actionTypes.REMOVE_DISH_ITEM:
      let dishList = {...state.dishList};
      delete dishList[action.key];
      return ({dishList});
    default:
      return state;
  }
};

export default reducer;