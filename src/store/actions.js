import * as actionTypes from './actionTypes';
import axios from '../axios-pizzeria';

export const loadDishesList = dishList => {
  return {type: actionTypes.LOAD_DISHES_LIST, dishList};
};

export const removeDishItem = key => {
  return {type: actionTypes.REMOVE_DISH_ITEM, key};
};

export const loadOrdersList = orders => {
  return {type: actionTypes.LOAD_ORDERS_LIST, orders};
};

export const completeExistingOrder = id => {
  return {type: actionTypes.COMPLETE_EXISTING_ORDER, id};
};

export const addNewDish = newDish => {
  return dispatch => {
    axios.post('dishes.json', newDish).then(() => {
      dispatch(loadDishes());
    }).catch(error => console.log(error));
  };
};

export const loadDishes = () => {
  return dispatch => {
    axios.get('dishes.json').then(response => {
      dispatch(loadOrders());
      dispatch(loadDishesList(response.data));
    }).catch(error => console.log(error));
  }
};

export const removeDish = key => {
  return dispatch => {
    axios.delete(`dishes/${key}.json`).finally(dispatch(removeDishItem(key)));
  }
};

export const editDish = (key, dish) => {
  return dispatch => {
    axios.put(`dishes/${key}.json`, dish).then(() => {
      dispatch(loadDishes());
    }).catch(error => console.log(error));
  }
};

export const loadOrders = () => {
  return dispatch => {
    axios.get('orders.json').then(response => {
      dispatch(loadOrdersList(response.data));
    }).catch(error => console.log(error));
  }
};

export const completeOrder = id => {
  return dispatch => {
    axios.delete(`orders/${id}.json`).finally(dispatch(completeExistingOrder(id)));
  };
};