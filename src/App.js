import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import DishesList from "./containers/DishesList/DishesList";
import OrdersList from "./containers/OrdersList/OrdersList";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={DishesList} />
          <Route path="/orders" component={OrdersList} />
          <Route render={() => <h1>404 Not found</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;