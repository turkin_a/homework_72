import React, {Component} from 'react';

import './NewDishForm.css';
import Button from "../UI/Button/Button";

class NewDishForm extends Component {
  state = {
    newDish: {
      name: '',
      url: '',
      cost: ''
    },
    mayBeCreate: false
  };

  componentDidMount() {
    if (this.props.dish) this.setState({newDish: this.props.dish});
  }

  changeInputHandler = event => {
    const key = event.target.name;
    let newDish = {...this.state.newDish};
    newDish[key] = event.target.value;
    let mayBeCreate = this.state.mayBeCreate;

    if (key === 'cost') {
      if (parseInt(newDish[key], 10) < 0) return null;
      else mayBeCreate = this.state.newDish.name.length > 0;
    } else if (key === 'name') mayBeCreate = (newDish[key].length > 0 && this.state.newDish.cost.length > 0);

    this.setState({newDish, mayBeCreate});
  };

  render() {
    return (
      <div className="NewDishForm">
        <form className="Form">
          <div className="FormRow">
            <label><span className="LabelName">Title:</span>
              <input type="text" name="name"
                     value={this.state.newDish.name}
                     onChange={(e) => this.changeInputHandler(e)}
              />
            </label>
          </div>
          <div className="FormRow">
            <label><span className="LabelName">URL:</span>
              <input type="text" name="url"
                     value={this.state.newDish.url}
                     onChange={(e) => this.changeInputHandler(e)}
              />
            </label>
          </div>
          <div className="FormRow">
            <label><span className="LabelName">Price:</span>
              <input type="number" name="cost"
                     value={this.state.newDish.cost}
                     onChange={(e) => this.changeInputHandler(e)}
              />
            </label>
          </div>
          <div className="FormRow FormRow-buttons">
            <Button disabled={!this.state.mayBeCreate}
                    classes={['WithoutBorder', 'Submit']}
                    clicked={(e) => this.props.clicked(e, {...this.state.newDish}, this.props.id)}
            >{this.props.okBtn}</Button>
            <Button classes={['WithoutBorder', 'Danger']}
                    clicked={(e) => this.props.clicked(e)}
            >CANCEL</Button>
          </div>
        </form>
      </div>
    )
  }
}

export default NewDishForm;