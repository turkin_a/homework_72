import React from 'react';
import './DishItem.css';

const DishItem = props => {
  return (
    <div className="DishItem">
      <div className="DishImage">
        <img src={props.dish.url} alt={props.dish.name}/>
        {props.dish.url}
      </div>
      <div className="DishInfo">
        <span className="DishName">{props.dish.name}</span>
        <span className="DishCost">Cost <strong>{props.dish.cost}</strong> KGS</span>
      </div>
      <div className="DishControls">
        <span className="Edit" onClick={props.editDish}>Edit</span>
        <span className="Delete" onClick={props.removeDish}>Delete</span>
      </div>
    </div>
  )
};

export default DishItem;